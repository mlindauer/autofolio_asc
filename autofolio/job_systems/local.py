'''
Created on Dec 11, 2014

@author: Marius Lindauer
'''

from subprocess import Popen
import multiprocessing
import Queue
import logging

class LocalExecutor(object):
    '''
    classdocs
    '''


    def __init__(self, cores=2):
        '''
        Constructor
        '''
        
        self.cores = cores
        
    def submit_jobs(self, calls):
        ''' 
            run on local machine command line <calls> in  parallel with at most <self.cores> processes at a time
        '''
        
        queue = multiprocessing.Queue()
        for c in calls:
            queue.put(c)
        procs = [multiprocessing.Process(target = self._work, args = (queue,i,)) for i in xrange(self.cores)]
        
        for p in procs:
            p.start()
        for p in procs:
            p.join()
            
    def _work(self, queue, id):
        while True:
            try:
                cmd = queue.get(block=False)
            except Queue.Empty:
                break
            logging.info(cmd)
            p = Popen(cmd, shell=True)
            p.communicate()
        
        
        